import csv
import os
import sys
import pandas as pd
import numpy as  np
# err cic.cs: sudo apt install libpcap-dev

dir_ = './pcap/'
save_dir_ ='./csv/'
for folder in os.listdir(dir_):
    if folder.endswith('.pcap') == True:
        os.system('sh convert_pcap_csv.sh '+dir_+folder)
        continue
    if os.path.exists(save_dir_ +folder) == False:
        os.mkdir(save_dir_ +folder)
    dir_folder = dir_ + folder +'/'
    for fd in os.listdir(dir_folder):
        if fd.endswith('.pcap') == False:
        # try:
            if os.path.exists(save_dir_ +folder +'/'+fd) == False:
                os.mkdir(save_dir_ +folder +'/'+fd)
            for file in os.listdir(dir_folder+fd):
                if file.endswith('.pcap'):
                    os.system('sh convert_pcap_csv.sh '+dir_folder+fd+'/'+file +' '+folder+'/'+fd)
        # except:
        else:
            os.system('sh convert_pcap_csv.sh '+dir_folder+fd+' '+folder)
